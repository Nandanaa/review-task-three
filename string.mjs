export function charcount(str, letter) {
    let count = 0;
    str=str.toLowerCase();
    letter=letter.toLowerCase();
    for (let i = 0; i < str.length; i++)
    {

        if (str[i] == letter)
        {
            count += 1;
        }
    }
    return count;
}
let a=process.argv[2];
let b=process.argv[3];

const result = charcount(a,b);
console.log(`Number of times the character occur in the string is ${result}`);
