export function combination(n, r)
{

    return (fact(n) /(fact(r)* fact(n - r)));
}

export function fact(n)
{
    let res = 1
    for (let i = 2; i <=n; i++)
    {
     res = res * i;
    }
    return res;

}
var myArgs = process.argv.slice(2);
let a = myArgs[0];
let b = myArgs[1];
let result = combination(a,b);
console.log(`The result is ${result}`);
