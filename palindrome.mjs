export function palindrome(str) {

    const rev = str.split('').reverse().join('');
    console.log(`The reversed string is ${rev}`);
    return str === rev;
}

let string= process.argv[2];
let result=palindrome(string);

if(result === true)
{
  console.log("given string is palindrome");
}
else {
  console.log("given string is not palindrome");
}
